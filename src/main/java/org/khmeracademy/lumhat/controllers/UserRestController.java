package org.khmeracademy.lumhat.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {
    Map<String,Object> map=new HashMap<>();
    HttpStatus httpStatus=null;
    @GetMapping
    public ResponseEntity<Map<String,Object>> findAllUser()
    {
        try {
//
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","success!");
                map.put("status",true);
                map.put("data","" );
                map.put("pagination","");

        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus.OK);
    }
}
