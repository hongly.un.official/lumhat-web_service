package org.khmeracademy.lumhat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumhatApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumhatApplication.class, args);
    }

}

