package org.khmeracademy.lumhat.services.category;



import org.khmeracademy.lumhat.models.Major;
import org.khmeracademy.lumhat.models.SubMajor;
import org.khmeracademy.lumhat.models.Subject;

import java.util.List;

public interface CategoryService {
    List<Major> findAllMajor();
    List<SubMajor> findAllSubMajor();
    List<SubMajor> findAllSubject();
    Major findMajorById(int id);
    List<Subject> findSubjectsByMajorId(int id);
}
