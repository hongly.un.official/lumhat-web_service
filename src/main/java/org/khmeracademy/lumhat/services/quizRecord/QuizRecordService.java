package org.khmeracademy.lumhat.services.quizRecord;


import org.khmeracademy.lumhat.models.QuizRecord;

public interface QuizRecordService {
    void insert(QuizRecord quizRecord);
    void deleteAllRecord(Integer userId);

}
