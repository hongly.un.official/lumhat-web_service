package org.khmeracademy.lumhat.services.admin.quiz;


import org.khmeracademy.lumhat.models.Answer;
import org.khmeracademy.lumhat.models.Instruction;
import org.khmeracademy.lumhat.models.Question;
import org.khmeracademy.lumhat.models.Quiz;

public interface AdminQuizService {

    void insertQuiz(Quiz quiz);
    void insertInstruction(Instruction instruction);
    void insertQuestion(Question question);
    void insertAnswer(Answer answer);
    Quiz findQuizById(Integer id);
    Boolean insertAllDataIntoDatabase(Quiz quiz);
    Boolean deleteQuestionByQuestionID(Integer id);
    Boolean deleteQuizByID(Integer id);
    Boolean deleteAnswerByQuestionID(Integer id);
    Boolean updateQuestionByID(Question question);
}
