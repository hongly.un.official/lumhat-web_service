package org.khmeracademy.lumhat.services.admin;

import org.khmeracademy.lumhat.models.Role;
import org.khmeracademy.lumhat.models.form.RoleForm;
import org.khmeracademy.lumhat.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminRoleServiceImp implements AdminRoleService{

    private RoleRepository roleRepository;
    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> findRoleByUserId(int userId) {
        return null;
    }

    @Override
    public void updateRole(RoleForm roleForm) {
        roleRepository.updateRole(roleForm);
    }
}
