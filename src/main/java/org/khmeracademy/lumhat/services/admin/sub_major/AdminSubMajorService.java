package org.khmeracademy.lumhat.services.admin.sub_major;

import org.khmeracademy.lumhat.models.SubMajor;
import org.khmeracademy.lumhat.models.form.SubMajorForm;

import java.util.List;

public interface AdminSubMajorService {
    List<SubMajor> findAll();
    SubMajor findOne(int id);
    void add(SubMajorForm subMajorForm);
    void update(SubMajorForm subMajorForm);
    void delete(int id);
}
