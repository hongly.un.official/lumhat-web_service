package org.khmeracademy.lumhat.services.admin.subject;

import org.khmeracademy.lumhat.models.Subject;
import org.khmeracademy.lumhat.models.form.SubjectForm;

import java.util.List;

public interface AdminSubjectService {
    void add(SubjectForm subjectForm);
    void update(SubjectForm subjectForm);
    List<Subject>findAllSubject();
    void delete(int id);
}
