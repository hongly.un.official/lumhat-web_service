package org.khmeracademy.lumhat.services.admin;

import org.khmeracademy.lumhat.models.Role;
import org.khmeracademy.lumhat.models.form.RoleForm;

import java.util.List;

public interface AdminRoleService {
    List<Role> findRoleByUserId(int userId);
    void updateRole(RoleForm roleForm);
}
