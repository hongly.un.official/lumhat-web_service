package org.khmeracademy.lumhat.services.admin.major;


import org.khmeracademy.lumhat.models.Major;

import java.util.List;

public interface AdminMajorService {
    List<Major> findAll();
    Major findOne(int id);
    void add(Major major);
    void update(Major major);
    void delete(int id);
}
