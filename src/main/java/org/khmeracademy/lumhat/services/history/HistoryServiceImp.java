package org.khmeracademy.lumhat.services.history;

import org.khmeracademy.lumhat.models.History;
import org.khmeracademy.lumhat.repositories.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@EnableAsync
public class HistoryServiceImp implements HistoryService {

    private HistoryRepository historyRepository;

    @Autowired
    public void setHistoryRepository(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Override
    public List<History> findHistoryByUserId(Integer userId) {
        List<History> histories = historyRepository.findHistoryByUserId(userId);
        for(int i=0 ; i<histories.size() ; i++) {
            histories.get(i).setQuizRecords(historyRepository.findQuizRecordByQuizId(histories.get(i).getSubMajor().getId(), userId));
        }
        return histories;
    }
}
