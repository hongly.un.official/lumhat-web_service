package org.khmeracademy.lumhat.services.history;

import org.khmeracademy.lumhat.models.History;

import java.util.List;

public interface HistoryService {
    List<History> findHistoryByUserId(Integer userId);

}
