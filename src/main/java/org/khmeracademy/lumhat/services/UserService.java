package org.khmeracademy.lumhat.services;

import org.khmeracademy.lumhat.models.User;
import org.khmeracademy.lumhat.models.utility.Paging;

import java.util.List;

public interface UserService {
    boolean add(User user);

     User findOne(String id);

    List<User> findAll();

    boolean delete(String id);

    boolean update(User user);

     String findById(String id);

     //Counting question show on Dashboard admin
    String findQtyUser();
    String findAllQuiz();
    String findItQuestion();
    String findEnglishQuestion();
    String findKoreanQuestion();
    List<User> findAllUser(Paging paging);
    int countUser();

}
