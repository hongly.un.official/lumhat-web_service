package org.khmeracademy.lumhat.services.question;


import org.khmeracademy.lumhat.models.Instruction;
import org.khmeracademy.lumhat.models.QuestionFilter;

import java.util.List;

public interface QuestionService {
    List<Instruction> findInstructionByQuizId(QuestionFilter questionFilter);
}
