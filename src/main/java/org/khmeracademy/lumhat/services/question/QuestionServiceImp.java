package org.khmeracademy.lumhat.services.question;

import org.khmeracademy.lumhat.models.Instruction;
import org.khmeracademy.lumhat.models.QuestionFilter;
import org.khmeracademy.lumhat.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImp implements QuestionService{

    private QuestionRepository questionRepository;

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Instruction> findInstructionByQuizId(QuestionFilter questionFilter) {
        return questionRepository.findInstructionByQuizId(questionFilter);
    }
}
