package org.khmeracademy.lumhat.services;


import org.khmeracademy.lumhat.models.User;
import org.khmeracademy.lumhat.models.utility.Paging;
import org.khmeracademy.lumhat.repositories.UserRepository;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@MapperScan("com.kshrd.app.repository")
public class UserServiceImp  implements UserService  {

    private UserRepository userRepo;
    @Autowired
    public void setUserRepo(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public boolean add(User user)
    {
        return userRepo.add(user);
    }

    @Override
    public User findOne(String id)
    {
        return userRepo.findOne(id);
    }

    @Override
    public List<User> findAll()
    {
        return userRepo.findAll();
    }

    @Override
    public boolean delete(String id)
    {
        return userRepo.delete(id);
    }

    @Override
    public boolean update(User user)
    {
        return userRepo.update(user);
    }

    @Override
    public String findById(String id) {
        return userRepo.findById(id);
    }

    @Override
    public String findQtyUser() {
        return userRepo.findQtyUser();
    }

    @Override
    public String findAllQuiz() {
        return userRepo.findAllQuiz();
    }

    @Override
    public String findItQuestion() {
        return userRepo.findItQuestion();
    }

    @Override
    public String findEnglishQuestion() {
        return userRepo.findEnglishQuestion();
    }

    @Override
    public String findKoreanQuestion() {
        return userRepo.findKoreanQuestion();
    }

    @Override
    public List<User> findAllUser(Paging paging) {
        return userRepo.findAllUser(paging);
    }

    @Override
    public int countUser() {
        return userRepo.countUser();
    }
}
