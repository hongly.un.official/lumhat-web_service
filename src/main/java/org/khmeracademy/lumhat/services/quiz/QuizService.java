package org.khmeracademy.lumhat.services.quiz;



import org.khmeracademy.lumhat.models.Level;
import org.khmeracademy.lumhat.models.Quiz;

import java.util.List;

public interface QuizService {
    List<Level>findLevelByMajorId(Integer id);
    List<Quiz> findAllQuiz();
    List<Quiz> findQuizById(Integer id);
}
