package org.khmeracademy.lumhat.services.quiz;

import org.khmeracademy.lumhat.models.Level;
import org.khmeracademy.lumhat.models.Quiz;
import org.khmeracademy.lumhat.repositories.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizServiceImp implements QuizService {
    private QuizRepository quizRepository;

    @Autowired
    public void setQuizRepository(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    @Override
    public List<Level> findLevelByMajorId(Integer id) {
        return quizRepository.findLevelBySubMajorId(id);
    }

    @Override
    public List<Quiz> findAllQuiz() {
        return quizRepository.findAllQuiz();
    }

    @Override
    public List<Quiz> findQuizById(Integer id) {
        return quizRepository.findQuizById(id);
    }
}
