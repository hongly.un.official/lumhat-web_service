package org.khmeracademy.lumhat.configurations;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@MapperScan("org.khmeracademy.lumhat.repositories")
@PropertySource("classpath:application.properties")
public class DatabaseConfiguration {
    @Value("${spring.datasource.driver-class-name}")
    String className;
    @Value("${spring.datasource.url}")
    String url;
    @Value("${spring.datasource.username}")
    String username;
    @Value("${spring.datasource.password}")
    String password;
    @Bean
    @Qualifier("dataSource")
    public DataSource productionDataSource() {

        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setDriverClassName(className);
        db.setUrl(url);
        db.setUsername(username);
        db.setPassword(password);
        return db;
    }
}
